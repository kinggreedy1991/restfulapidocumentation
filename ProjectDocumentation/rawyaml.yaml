swagger: '2.0'
info:
  version: 1.0.0
  title: RESTful API for Roundup
  description: |
    This is the documentation for Roundup RESTful API.
    If you want to use 'Try the operation', please check the Full documentation.
basePath: /rest
schemes:
  - http
securityDefinitions:
  BasicAuth:
    type: basic
    description: HTTP Basic Authentication. Login credentials are the same as your Roundup login credentials.
paths:
  '/data/{class}':
    get:
      tags:
        - Collection
      summary: |
        Get the list of objects from a Class.
      operationId: get_collection
      produces:
        - application/json
      parameters:
        - in: path
          name: class
          description: Name of the `Class`.
          required: true
          type: string
      responses:
        '200':
          description: Successful response
          schema:
            type: object
            properties:
              data:
                type: array
                items:
                  $ref: '#/definitions/item'
        '403':
          description: Insufficient Permission
          schema:
            type: object
            properties:
              error:
                $ref: '#/definitions/error'
        '404':
          description: Resource is not available
          schema:
            type: object
            properties:
              error:
                $ref: '#/definitions/error'
    post:
      tags:
        - Collection
      summary: |
        Add a new item to a Class
      description: |
        This method add a new item to the `Class`. The form data contains all of
        the attributes of the item. Only `required` attributes are required. The other attributes are
        optional
      operationId: post_collection
      consumes:
        - application/x-www-form-urlencoded
        - multipart/form-data
      produces:
        - application/json
      parameters:
        - in: path
          name: class
          description: Name of the `Class`.
          required: true
          type: string
        - in: query
          name: required_attribute
          description: Key attribute of the object
          required: true
          type: string
        - in: query
          name: optional_attribute_1
          description: 'Another optional attribute of the object'
          required: false
          type: string
        - in: query
          name: optional_attribute_2
          description: 'Another optional attribute of the object'
          required: false
          type: string
        - in: query
          name: optional_attribute_3
          description: 'Another optional attribute of the object'
          required: false
          type: string
      responses:
        '201':
          description: Successful response
          schema:
            title: data
            type: object
            properties:
              item:
                $ref: '#/definitions/item'
        '400':
          description: Malformed request
          schema:
            type: object
            properties:
              error:
                $ref: '#/definitions/error'
        '403':
          description: Insufficient Permission
          schema:
            type: object
            properties:
              error:
                $ref: '#/definitions/error'
        '404':
          description: Resource is not available
          schema:
            type: object
            properties:
              error:
                $ref: '#/definitions/error'
        '409':
          description: "The unique attribute's value is already exist."
          schema:
            type: object
            properties:
              error:
                $ref: '#/definitions/error'
      security:
        - BasicAuth: []
    delete:
      tags:
        - Collection
      summary: |
        Delete all objects of a Class
      description: |
        This method will delete all objects of a `Class`. However, notice that Roundup does not allow delete operation by default.
      operationId: delete_collection
      produces:
        - application/json
      parameters:
        - in: path
          name: class
          description: Name of the `Class`.
          required: true
          type: string
      responses:
        '200':
          description: Successful response
          schema:
            title: data
            type: object
            properties:
              status:
                type: string
              count:
                type: integer
                format: int32
        '403':
          description: Insufficient Permission
          schema:
            type: object
            properties:
              error:
                $ref: '#/definitions/error'
      security:
        - BasicAuth: []
    options:
      tags:
        - Collection
      summary: |
        Request for available communication options
      description: |
        Request for information of the available communication options on the `Class`. This method allows the client to determine the options and/or requirements associated with a resource, without implying a resource action or initiating a resource retrieval.
      operationId: options_collection
      produces:
        - application/json
      parameters:
        - in: path
          name: class
          description: Name of the `Class`.
          required: true
          type: string
      responses:
        '204':
          description: Successful response
        '404':
          description: Resource is not available
          schema:
            type: object
            properties:
              error:
                $ref: '#/definitions/error'
  '/data/{class}/{id}':
    get:
      tags:
        - Element
      summary: |
        Get an Object from a Class.
      operationId: get_element
      parameters:
        - in: path
          name: class
          description: Name of the `Class`
          required: true
          type: string
        - in: path
          name: id
          description: Id of the `Object`
          required: true
          type: string
      responses:
        '200':
          description: Successful response
          schema:
            type: object
            properties:
              data:
                $ref: '#/definitions/object'
        '403':
          description: Insufficient Permission
          schema:
            type: object
            properties:
              error:
                $ref: '#/definitions/error'
        '404':
          description: Resource is not available
          schema:
            type: object
            properties:
              error:
                $ref: '#/definitions/error'
    put:
      tags:
        - Element
      summary: |
        Replace data of the Object
      description:
        This method replace the data of the `Object` by the form data. If an optional field is omitted, the corresponding attribute will not be updated. This method will return only attributes that have been updated
      operationId: put_element
      consumes:
        - application/x-www-form-urlencoded
        - multipart/form-data
      produces:
        - application/json
      parameters:
        - in: path
          name: class
          description: Name of the `Class`.
          required: true
          type: string
        - in: path
          name: id
          description: Id of the `Object`
          required: true
          type: string
        - in: query
          name: key_attribute
          description: Key attribute of the object
          required: true
          type: string
        - in: query
          name: required_attribute
          description: Required attribute of the object
          required: true
          type: string
        - in: query
          name: optional_attribute_1
          description: 'Another attribute of the object, not required'
          required: false
          type: string
        - in: query
          name: optional_attribute_2
          description: 'Another attribute of the object, not required'
          required: false
          type: string
        - in: query
          name: optional_attribute_3
          description: 'Another attribute of the object, not required'
          required: false
          type: string
      responses:
        '200':
          description: Successful response
          schema:
            type: object
            properties:
              data:
                $ref: '#/definitions/object'
        '400':
          description: Malformed request
          schema:
            type: object
            properties:
              error:
                $ref: '#/definitions/error'
        '403':
          description: Insufficient Permission
          schema:
            type: object
            properties:
              error:
                $ref: '#/definitions/error'
        '404':
          description: Resource is not available
          schema:
            type: object
            properties:
              error:
                $ref: '#/definitions/error'
        '409':
          description: "The unique attribute's value is already exist."
          schema:
            type: object
            properties:
              error:
                $ref: '#/definitions/error'
      security:
        - BasicAuth: []
    delete:
      tags:
        - Element
      summary: |
        Delete an Object from a Class
      description: |
        This method will delete an `Object` from a `Class`. However, notice that Roundup does not allow delete operation by default.
      operationId: delete_element
      produces:
        - application/json
      parameters:
        - in: path
          name: class
          description: Name of the `Class`.
          required: true
          type: string
        - in: path
          name: id
          description: Id of the `Object`
          required: true
          type: string
      responses:
        '200':
          description: Successful response
          schema:
            title: data
            type: object
            properties:
              status:
                type: string
        '403':
          description: Insufficient Permission
          schema:
            type: object
            properties:
              error:
                $ref: '#/definitions/error'
      security:
        - BasicAuth: []
    patch:
      tags:
        - Element
      summary: |
        Patch an Object of a Class
      description:
        This method will patch an `Object` of the `Class` by performing `ADD`, `REMOVE`, `REPLACE` or `ACTION`. <br>
        `ADD` will append new data from the form data to the corresponding attribute. <br>
        `REMOVE` will clear the corresponding attribute, regardless of the value from the form data. <br>
        `REPLACE` will replace the corresponding attribute with the new value from the form data. <br>
        `ACTION` will perform an action (`Retire` for example) to the Object, and pass any form fields starting with `action_args` to the Action arguments.
      operationId: patch_element
      consumes:
        - application/x-www-form-urlencoded
        - multipart/form-data
      produces:
        - application/json
      parameters:
        - in: path
          name: class
          description: Name of the `Class`.
          required: true
          type: string
        - in: path
          name: id
          description: Id of the `Object`
          required: true
          type: string
        - in: query
          name: op
          description: |
            PATCH operators - `ADD` `REMOVE` `REPLACE` or `ACTION`. Default is `REPLACE`
          required: false
          type: string
        - in: query
          name: action_name
          required: true
          type: string
        - in: query
          name: key_attribute
          description: Key attribute of the object
          required: true
          type: string
        - in: query
          name: optional_attribute_1
          description: 'Another attribute of the object, not required'
          required: false
          type: string
        - in: query
          name: optional_attribute_2
          description: 'Another attribute of the object, not required'
          required: false
          type: string
        - in: query
          name: optional_attribute_3
          description: 'Another attribute of the object, not required'
          required: false
          type: string
      responses:
        '200':
          description: Successful response
          schema:
            type: object
            properties:
              data:
                $ref: '#/definitions/object'
        '400':
          description: Malformed request
          schema:
            type: object
            properties:
              error:
                $ref: '#/definitions/error'
        '403':
          description: Insufficient Permission
          schema:
            type: object
            properties:
              error:
                $ref: '#/definitions/error'
        '404':
          description: Resource is not available
          schema:
            type: object
            properties:
              error:
                $ref: '#/definitions/error'
        '409':
          description: "The unique attribute's value is already exist."
          schema:
            type: object
            properties:
              error:
                $ref: '#/definitions/error'
      security:
        - BasicAuth: []
    options:
      tags:
        - Element
      summary: |
        Request for available communication options
      description: |
        Request for information of the available communication options on the `Object` of the `Class`. This method allows the client to determine the options and/or requirements associated with a resource, without implying a resource action or initiating a resource retrieval.
      produces:
        - application/json
      parameters:
        - in: path
          name: class
          description: Name of the `Class`.
          required: true
          type: string
        - in: path
          name: id
          description: Id of the `Object`
          required: true
          type: string
      responses:
        '204':
          description: Successful response
        '404':
          description: Resource is not available
          schema:
            type: object
            properties:
              error:
                $ref: '#/definitions/error'
  '/data/{class}/{id}/{attr}':
    get:
      tags:
        - Attribute
      summary: |
        Get an object's attribute.
      operationId: get_attribute
      parameters:
        - in: path
          name: class
          description: Name of the `Class`
          required: true
          type: string
        - in: path
          name: id
          description: Id of the `Object`
          required: true
          type: string
        - in: path
          name: attr
          description: Attribute of the `Object`
          required: true
          type: string
      responses:
        '200':
          description: Successful response
          schema:
            type: object
            properties:
              data:
                $ref: '#/definitions/attr'
        '403':
          description: Insufficient Permission
          schema:
            type: object
            properties:
              error:
                $ref: '#/definitions/error'
        '404':
          description: Resource is not available
          schema:
            type: object
            properties:
              error:
                $ref: '#/definitions/error'
    put:
      tags:
        - Attribute
      summary: |
        Replace an Attribute of the Object
      description:
        This method replace the data of an `Attribute` by the form field `data`. This method will return only attributes that have been updated.
      operationId: put_attribute
      consumes:
        - application/x-www-form-urlencoded
        - multipart/form-data
      produces:
        - application/json
      parameters:
        - in: path
          name: class
          description: Name of the `Class`.
          required: true
          type: string
        - in: path
          name: id
          description: Id of the `Object`
          required: true
          type: string
        - in: path
          name: attr
          description: Attribute of the `Object`
          required: true
          type: string
        - in: query
          name: data
          description: New data for the attribute
          required: true
          type: string
      responses:
        '200':
          description: Successful response
          schema:
            type: object
            properties:
              data:
                $ref: '#/definitions/object'
        '400':
          description: Malformed request
          schema:
            type: object
            properties:
              error:
                $ref: '#/definitions/error'
        '403':
          description: Insufficient Permission
          schema:
            type: object
            properties:
              error:
                $ref: '#/definitions/error'
        '404':
          description: Resource is not available
          schema:
            type: object
            properties:
              error:
                $ref: '#/definitions/error'
        '409':
          description: "The unique attribute's value is already exist."
          schema:
            type: object
            properties:
              error:
                $ref: '#/definitions/error'
      security:
        - BasicAuth: []
    delete:
      tags:
        - Attribute
      summary: |
        Clear the Attribute's value of an Object
      description: |
        This method will set an attribute of an `Object` to empty list, dict or None based on its type.
      operationId: delete_attribute
      produces:
        - application/json
      parameters:
        - in: path
          name: class
          description: Name of the `Class`.
          required: true
          type: string
        - in: path
          name: id
          description: Id of the `Object`
          required: true
          type: string
        - in: path
          name: attr
          description: Attribute of the `Object`
          required: true
          type: string
      responses:
        '200':
          description: Successful response
          schema:
            title: data
            type: object
            properties:
              status:
                type: string
        '403':
          description: Insufficient Permission
          schema:
            type: object
            properties:
              error:
                $ref: '#/definitions/error'
      security:
        - BasicAuth: []
    patch:
      tags:
        - Attribute
      summary: |
        Patch the Attribute of the Object
      description:
        This method will patch an `Attribute` of the `Object` by performing `ADD`, `REMOVE` or `REPLACE`.
      operationId: patch_attribute
      consumes:
        - application/x-www-form-urlencoded
        - multipart/form-data
      produces:
        - application/json
      parameters:
        - in: path
          name: class
          description: Name of the `Class`.
          required: true
          type: string
        - in: path
          name: id
          description: Id of the `Object`
          required: true
          type: string
        - in: path
          name: attr
          description: Attribute of the `Object`
          required: true
          type: string
        - in: query
          name: op
          description: |
            PATCH operators - `ADD` `REMOVE` `REPLACE`. Default is `REPLACE`
          required: false
          type: string
        - in: query
          name: data
          description: New data for the attribute
          required: true
          type: string
      responses:
        '200':
          description: Successful response
          schema:
            type: object
            properties:
              data:
                $ref: '#/definitions/object'
        '400':
          description: Malformed request
          schema:
            type: object
            properties:
              error:
                $ref: '#/definitions/error'
        '403':
          description: Insufficient Permission
          schema:
            type: object
            properties:
              error:
                $ref: '#/definitions/error'
        '404':
          description: Resource is not available
          schema:
            type: object
            properties:
              error:
                $ref: '#/definitions/error'
        '409':
          description: "The unique attribute's value is already exist."
          schema:
            type: object
            properties:
              error:
                $ref: '#/definitions/error'
      security:
        - BasicAuth: []
    options:
      tags:
        - Attribute
      summary: |
        Request for available communication options
      description: |
        Request for information of the available communication options on the `Attribute` of the `Object`. This method allows the client to determine the options and/or requirements associated with a resource, without implying a resource action or initiating a resource retrieval.
      produces:
        - application/json
      parameters:
        - in: path
          name: class
          description: Name of the `Class`.
          required: true
          type: string
        - in: path
          name: id
          description: Id of the `Object`
          required: true
          type: string
        - in: path
          name: attr
          description: Attribute of the `Object`
          required: true
          type: string
      responses:
        '204':
          description: Successful response
        '404':
          description: Resource is not available
          schema:
            type: object
            properties:
              error:
                $ref: '#/definitions/error'
definitions:
  item:
    properties:
      id:
        type: string
      link:
        type: string
  object:
    properties:
      id:
        type: string
      type:
        type: string
      link:
        type: string
      attributes:
        type: object
  attr:
    properties:
      id:
        type: string
      type:
        type: string
      link:
        type: string
      data:
        type: string
  error:
    properties:
      status:
        type: integer
        format: int32
      msg:
        type: string
